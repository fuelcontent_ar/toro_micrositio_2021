const Colors = {
  black: '#232324',
  blue: '#18131F',
  danger: '#ff4757',
  grey: '#413C3A',
  greyDark: '#575757',
  greyMedium: '#edede3',
  light: '#EDEDE2',
  primary: '#FF1430',
  secondary: '#FFAF00',
  success: '#ff1430',
  warning: '#fffaeb',
  white: '#ffffff',
  default: '#000000',
  yellow: '#F2D788',
  shadown: '#213A6B',
  brown: '#34312F',
  zombie: '#E5C797',
  green: '#39D57B',
};

export default Colors;
