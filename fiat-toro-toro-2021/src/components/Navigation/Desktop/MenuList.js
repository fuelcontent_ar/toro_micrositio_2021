const MenuList = [
  {
    id: 1,
    name: 'Novedades Toro',
    ref: 'novidades-toro',
    link: 'toro-2022',
  },
  /*   {
    id: 2,
    name: 'Serviços Conectados',
    ref: 'servicos-conectados',
    link: 'services',
  }, */
  {
    id: 3,
    name: 'Versiones',
    ref: 'versoes',
    link: 'versions',
  },
  {
    id: 4,
    name: 'Fotos',
    ref: 'fotos',
    link: 'galeriafotos',
  },
  {
    id: 5,
    name: 'Todo sobre la Toro',
    ref: 'tudo-sobre-a-toro',
    link: 'tudo-sobre-a-toro',
    submenu: [
      {
        id: 1,
        name: 'Diseño',
        ref: 'design',
        link: 'Diseño',
      },
      {
        id: 2,
        name: 'Interior',
        ref: 'interior',
        link: 'Interior',
      },
      {
        id: 3,
        name: 'Performance',
        ref: 'performance',
        link: 'Performance',
      },
      {
        id: 4,
        name: 'Tecnología',
        ref: 'tecnologia',
        link: 'Tecnología',
      },
      {
        id: 5,
        name: 'Seguridad',
        ref: 'segurança',
        link: 'Seguridad',
      },

      {
        id: 6,
        name: 'Accesorios',
        ref: 'acessorios',
        link: 'Accesorios',
      },
      {
        id: 7,
        name: 'Paquete de servicios',
        ref: 'pacote-de-servicos',
        link: 'Paquete de servicios',
      },
    ],
  },
];

export default MenuList;
