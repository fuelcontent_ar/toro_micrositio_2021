import React, {
  useCallback,
  useEffect,
  useState,
  createRef,
} from 'react';
import Stories from 'react-insta-stories';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import { FiChevronRight } from 'react-icons/fi';
import {
  Container,
  Next,
  Content,
  Description,
  FiatTitle,
  FiatTitleStories,
  FiatToro,
  HandleIcon,
  LogoConnect,
} from './styles';
import { Colors } from '../../styles';
import * as images from '../../assets';

const StoriesComponent = ({ stories, defaultInterval }) => {
  const containerRef = createRef();

  useEffect(() => {
    const { current: container } = containerRef;
    const { children: containerChildren } = container || {};
    const [storiesContainer] = containerChildren || [];
    const { children: storiesChildren } = storiesContainer || {};
    const [storiesHeader] = storiesChildren || [];
    const { children: storiesHeaderChildren } =
      storiesHeader || {};

    // Condicional para trocar a cor do header dos stories
    if (storiesHeaderChildren && storiesHeaderChildren.length) {
      // eslint-disable-next-line no-restricted-syntax
      for (const item of storiesHeaderChildren) {
        const { children: itemChildren } = item || {};
        item.style.height = '5px';
        item.style.marginRight = '4px';
        item.style.marginTop = '7px';
        item.style.borderRadius = 0;

        const [itemComponent] = itemChildren || [];
        itemComponent.style.background = Colors.primary;
        itemComponent.style.borderRadius = 0;
      }
    }
  }, [containerRef]);

  const [screens, setScreens] = useState([]);
  const [current, setCurrent] = useState(0);

  const renderScreen = useCallback(() => {
    stories.map(item => {
      const screen = {
        content: () => (
          <Container id="toro-2022" background={item?.image}>
            <FiatTitle
              highlighted={item?.highlighted}
              width={item?.head?.width}
            >
              <FiatTitleStories>
                <h2>{item?.head?.titlemain}</h2>
                <h3>{item?.head?.titlenew} </h3>
               
                <h3>{item?.head?.brand}</h3>
              </FiatTitleStories>
              <h1>{item?.head?.title}</h1>
            </FiatTitle>

            <Content highlighted={item?.highlighted}>
              <Description
                highlighted={item?.highlighted}
                width={item?.description?.width}
              >
                {item.head?.next !== 'Design Externo' ? (
                  <>
                    <img src={images.FlagGreen} alt="Fiat Green" />
                    <p>{item?.description?.text}</p>
                  </>
                ) : null}
              </Description>
              <Next align={item?.action?.align}>
                <span>{item?.action?.text}</span>
                <AnchorLink href="#services">
                  <img src={images.Seta} alt="seta"></img>
                </AnchorLink>

                {item?.id === 1 ? (
                  <HandleIcon>
                    <img src={images.HandIcon} alt="handle" />
                  </HandleIcon>
                ) : (
                  <>{item?.id !== 6 && <FiChevronRight />}</>
                )}
              </Next>
            </Content>
          </Container>
        ),
      };

      return setScreens(rest => [...rest, screen]);
    });
  }, [stories]);

  useEffect(() => {
    renderScreen();
  }, [renderScreen]);

  if (screens.length < 1) {
    return <h1>Cargando...</h1>;
  }

  return (
    <FiatToro id="toro-2022" ref={containerRef}>
      <Stories
        stories={screens}
        defaultInterval={defaultInterval ?? 15000}
        width="100vw"
        height="450px"
        onStoryStart={(s, st) => {
          const direction = () => {
            if (s > current) {
              return 'proximo';
            }

            setCurrent(s);
            return 'anterior';
          };

          switch (s) {
            case 0:
              return window.dataLayer.push({
                event: 'interaction',
                segment: 'landing-page',
                product: 'toro-2022',
                interactionType: 'clique',
                pageSection: 'conteudo',
                pageSubsection: 'stories',
                elementCategory: direction(),
                element: 'nova-fiat-toro-2022',
              });

            case 1:
              return window.dataLayer.push({
                event: 'interaction',
                segment: 'landing-page',
                product: 'toro-2022',
                interactionType: 'clique',
                pageSection: 'conteudo',
                pageSubsection: 'stories',
                elementCategory: direction(),
                element: `${stories[s].head.titlenew}-${stories[s].head.brand}-${stories[s].head.subtitle}`
                .replace(/\s/g, '-')
                  .normalize('NFD')
                  .replace(/[\u0300-\u036f]/g, '')
                  .toLocaleLowerCase(),
              });

            case 2:
              return window.dataLayer.push({
                event: 'interaction',
                segment: 'landing-page',
                product: 'toro-2022',
                interactionType: 'clique',
                pageSection: 'conteudo',
                pageSubsection: 'stories',
                elementCategory: direction(),
                element: `${stories[s].head.titlenew}-${stories[s].head.brand}-${stories[s].head.subtitle}`
                .replace(/\s/g, '-')
                .normalize('NFD')
                .replace(/[\u0300-\u036f]/g, '')
                .toLocaleLowerCase(),
              });

            case 3:
              return window.dataLayer.push({
                event: 'interaction',
                segment: 'landing-page',
                product: 'toro-2022',
                interactionType: 'clique',
                pageSection: 'conteudo',
                pageSubsection: 'stories',
                elementCategory: direction(),
                element: `${stories[s].head.titlenew}-${stories[s].head.brand}-${stories[s].head.subtitle}`
                .replace(/\s/g, '-')
                .normalize('NFD')
                .replace(/[\u0300-\u036f]/g, '')
                .toLocaleLowerCase(),
              });

            case 4:
              return window.dataLayer.push({
                event: 'interaction',
                segment: 'landing-page',
                product: 'toro-2022',
                interactionType: 'clique',
                pageSection: 'conteudo',
                pageSubsection: 'stories',
                elementCategory: direction(),
                element: `${stories[s].head.titlenew}-${stories[s].head.brand}-${stories[s].head.subtitle}`
                .replace(/\s/g, '-')
                .normalize('NFD')
                .replace(/[\u0300-\u036f]/g, '')
                .toLocaleLowerCase(),
              });
            case 5:
              return window.dataLayer.push({
                event: 'interaction',
                segment: 'landing-page',
                product: 'toro-2022',
                interactionType: 'clique',
                pageSection: 'conteudo',
                pageSubsection: 'stories',
                elementCategory: direction(),
                element: 'connect-me',
              });

            default:
              break;
          }

          return null;
        }}
        loop
      />
    </FiatToro>
  );
};

export default StoriesComponent;
