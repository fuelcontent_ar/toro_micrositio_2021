import styled, { css } from 'styled-components';
import { switchProp } from 'styled-tools';
import { Colors } from '../../styles';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 28px;
  width: 100%;
  height: 100%;

  ${props =>
    props.background &&
    css`
     background-image: url(${props.background});
     background-size: cover; 
     background-repeat: no-repeat;
     object-fit: cover;
     height: 100%;
    `};
`;


export const LogoConnect = styled.div`
    position:absolute;
    bottom: 73%;

`;


export const HandleIcon = styled.div`
  position:relative;
  right: 0;
  top: -20px;
  margin-left: 10px;
  margin-right: 5px;
`;

export const FiatTitle = styled.div`
  color: ${Colors.light};
  height: 127px;
  width: ${props => props.width && props.width}px;
  margin-top: ${props => (props.highlighted ? '0' : '20')}px;
  h1 {
    font-family: 'FuturaPT Bold', sans-serif;
    font-size: ${props => (props.highlighted ? '39' : '26')}px;
    font-weight: 600;
    letter-spacing: 0.29px;
    line-height: 33px;
    position: relative;
    line-height: 2rem;

    h3 {
      position: relative;
      z-index: 1;
      font-family: 'FuturaPT Bold', sans-serif;
    }
  }

`;

export const FiatTitleStories = styled.div`
  display: flex;
  align-items: center;

  h3 {
  font-size: ${props => (props.highlighted ? '20' : '26')}px;
  margin-right: 5px;
  font-family: 'FuturaPT Bold', sans-serif;
  }
  h3:last-child {
    color: ${Colors.primary}
  }

  h2 {
    font-size: 16px;
  }
`;

export const Content = styled.div`
  align-items: flex-${props => (props.highlighted ? 'end' : 'start')};
  display: flex;
  flex-direction: column;
`;

export const Description = styled.div`
  position: relative;
  width: 181px;

  img {
    display: flex;
    height: 30px;
    left: ${props => (props.highlighted ? '-37' : 0)}vw;
    position: absolute;
    top: ${props => (props.highlighted ? '-330' : '-170')}px;
    width: ${props => (props.highlighted ? '40' : '38')}px;
    z-index: 0;
  }

  p {
    color: ${Colors.light};
    display: flex;
    font-size: 15px;
    height: 92px;
    justify-content: flex-start;
    letter-spacing: 0.11px;
    line-height: 21px;
    position: relative;
    top: -170px;
    left: 60px;
    font-family: "FuturaPT Medium",sans-serif;
    width: 285px; 
  }
`;

export const Next = styled.div`
  color: ${Colors.light};
  display: flex;
  font-size: 16px;
  line-height: 18px;
  margin-top: 16px;
  width: 100%;

  span {
    font-family: 'FuturaPT Bold', sans-serif;
    position: relative;
    top: -6px;

    @media (min-width: 414px) {
      top: 0px;
    }
  }
  img {
    position:absolute;
    bottom: 0;
    left:50%;
    top: 96.5%;
    z-index: 1000;
  }
  svg {
    font-size: 22px;
    margin-left: 9px;
    position: relative;
    top: -8px;
  
  }

  a {
    z-index: 99999px;
  }

  ${switchProp('align', {
    center: css`
      margin-left: 10px;
      justify-content: center;
    
      svg {
        margin-left: 0;
      }
    `,
    left: css`
      justify-content: flex-start;
    `,
    right: css`
      justify-content: flex-end;
    `,
  })}
`;



export const FiatToro = styled.div`

  /* Iphone 5 */
  @media (min-width: 320px) and (min-height: 568px) {
    height: 450px;
  }
  /* Moto g4/s5 */
  @media (min-width: 360px) and (min-height: 640px) {
  height: 450px;
  }
  /* 700 Height */
  @media (min-width: 360px) and (min-height: 700px) {
    height: 450px;
  }
  /* S8 */
  @media (min-width: 360px) and (min-height: 730px) {
    height: 450px;
  }
  /* Iphone 6/7/8 */
  @media (min-width: 375px) and (min-height: 667px) {
   height: 450px;
  }
  /* Iphone x */
  @media (min-width: 375px) and (min-height: 812px) {
    height: 450px;
  }
  /* Pixel 2 */
  @media (min-width: 411px) and (min-height: 731px) {
    height: 450px;
  }
  /* Pixel 2 XL */
  @media (min-width: 411px) and (min-height: 823px) {
   height: 450px;
  }
  /* Iphone 6/7/8 Plus */
  @media (min-width: 414px) and (min-height: 736px) {
    height: 450px;
  }
`;