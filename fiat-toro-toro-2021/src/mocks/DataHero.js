import * as images from '../assets';

const DataHero = [
  {
    id: 1,
    image: images.hero.storiesIndex,
    highlighted: true,
    new: true,
    head: {
      title: '',
      Subtitle: '',
    },
  },
  {
    id: 2,
    new: true,
    image: images.hero.storiesMotor,
    head: {
      title: 'Motor',
      brand: 'turbo',
      Subtitle: 'T270 NAFTA',
      description:
        'Mucha más potencia con un torque de 270Nm a 1750 rpm con un menor consumo de combustible.',
    },
  },
  {
    id: 3,
    image: images.hero.storiesDesign,
    head: {
      title: 'Nuevo',
      brand: 'Diseño',
      Subtitle: 'externo',
      description:
        'Tiene tantas novedades de diseño que ni vas a saber qué te enamoró más.',
    },
  },
  {
    id: 4,
    image: images.hero.storiesInterior,
    head: {
      title: '',
      brand: 'Interior',
      Subtitle: 'totalmente renovado',
      description:
        'Tablero full digital de 7", cargador inalámbrico y mucho mas espacio para que guardes todo.',
    },
  },
  {
    id: 5,
    image: images.hero.storiesMultimidia,
    head: {
      title: 'Nueva',
      brand: 'Central',
      brand2: 'multimedia ',
      Subtitle: 'de 10,1"',
      description:
        'Modular y personalizable para que la configures a tu gusto. Además tiene GPS y Android auto / Apple Carplay con conexión inalámbrica.',
    },
  },
  {
    id: 6,
    new: true,
    image: images.hero.storiesConnect,
    head: {
      title: 'NUEVO',
      brand: 'ASISTENCIAS',
      Subtitle: 'A LA CONDUCCIÓN',
      description:
        'Asistente de frenado Pre-colisión Sistema automático de luces altas Alerta de cambio de carril involuntario.',
    },
  },
];

export default DataHero;
