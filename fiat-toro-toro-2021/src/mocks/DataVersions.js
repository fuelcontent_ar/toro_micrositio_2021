import * as images from '../assets';

const Versions = [
  {
    version: 'TORO ULTRA',
    presentation: {
      image: images.toroFreedom,
      title: 'ULTRA ',
      subtitle: 'DIESEL',
      width: 230,
      bg: images.bgUltra,
      download: images.DownloadUltra,
    },
    details: [
      {
        bg: images.CardCapota,
        text: 'Capota rígida',
      },
      {
        bg: images.CardSanto,
        text: 'Barra san Antonio integrada',
      },
      {
        bg: images.CardMultimidia,
        text: 'Central multimidia 10.1',
      },
    ],
    gallery: [
      {
        photos: images.GalleryU1,
        mini: true,
      },
      {
        photos: images.GalleryU2,
      },
      {
        photos: images.GalleryU3,
      },
      {
        photos: images.GalleryU4,
      },
      {
        photos: images.GalleryU5,
      },
      {
        photos: images.GalleryU6,
        mini: true,
      },
    ],
  },
  {
    version: 'TORO VOLCANO',
    presentation: {
      image: images.toroVolcano,
      title: 'VOLCANO',
      subtitle: 'DIESEL NAFTA',
      width: 230,
      bg: images.bgVolcano,
      download: images.DownloadVolcano,
    },
    details: [
      {
        bg: images.CardMultimidia2,
        text: 'ADAS',
      },
      {
        bg: images.CardWireless,
        text: 'Apertura y encendido remoto',
      },
      {
        bg: images.CardRodas,
        text: 'LLantas de aleación 18”',
      },
    ],
    gallery: [
      {
        photos: images.GalleryV1,
        mini: true,
      },
      {
        photos: images.GalleryV2,
      },
      {
        photos: images.GalleryV3,
      },
      {
        photos: images.GalleryV4,
      },
      {
        photos: images.GalleryV5,
      },
      {
        photos: images.GalleryV6,
        mini: true,
      },
    ],
  },
  {
    version: 'TORO FREEDOM',
    presentation: {
      image: images.toroFreedom,
      title: 'FREEDOM',
      subtitle: 'DIESEL NAFTA',
      width: 230,
      bg: images.bgFreedom,
      download: images.DownloadFreedom,
    },
    details: [
      {
        bg: images.CardFrontal,
        text: 'Cargador inalámbrico',
      },
      {
        bg: images.CardMultimidia8,
        text: 'Central multimedia 8,4”',
      },
      {
        bg: images.CardFarol,
        text: 'Faros Full LED',
      },
    ],
    gallery: [
      {
        photos: images.GalleryF1,
        mini: true,
      },
      {
        photos: images.GalleryF2,
      },
      {
        photos: images.GalleryF3,
      },
      {
        photos: images.GalleryF4,
      },
      {
        photos: images.GalleryF5,
      },
      {
        photos: images.GalleryF6,
        mini: true,
      },
    ],
  }
  
];

export default Versions;
