import * as images from '../assets';

const DataStories = [
  {
    id: 1,
    image: images.Stories01,
    imageAlt: 'Stories 1 - Image',
    highlighted: true,
    head: {
      width: 50,
      next: 'Design Externo',
    },
    action: {
      text: 'Toca y mira las novedades',
      align: 'right',
    },
  },
  {
    id: 2,
    image: images.Stories02,
    imageAlt: 'Stories 2 - Image',
    head: {
      titlenew: 'MOTOR',
      title: 'T270 NAFTA',
      width: 300,
      brand: 'TURBO',
      subtitle: 'T270 NAFTA',
    },
    description: {
      text:
        'Mucha más potencia con un torque de 270Nm a 1750 rpm con un menor consumo de combustible.',
    },
    action: {
      text: 'Diseño externo',
      align: 'right',
    },
  },
  {
    id: 3,
    image: images.Stories03,
    imageAlt: 'Stories 3 - Image',
    head: {
      titlenew: 'NUEVO',
      title: 'EXTERNO',
      width: 370,
      brand: 'DISEÑO',
      subtitle: 'EXTERNO',
    },
    description: {
      text:
        'Tiene tantas novedades de diseño que ni vas a saber qué te enamoró más.',
    },
    action: {
      text: 'Diseño interno',
      align: 'right',
    },
  },
  {
    id: 4,
    image: images.Stories04,
    imageAlt: 'Stories 4 - Image',
    head: {
      
      title: 'TOTALMENTE RENOVADO',
      width: 350,
      brand: 'INTERIOR',
      subtitle: 'TOTALMENTE RENOVADO',
    },

    description: {
      text:
        'Tablero full digital de 7", cargador inalámbrico y mucho mas espacio para que guardes todo.',
    },
    action: {
      text: 'Central multimedia',
      align: 'right',
    },
  },
  {
    id: 5,
    image: images.Stories05,
    imageAlt: 'Stories 5 - Image',
    head: {
      titlenew: 'NUEVA',
      title: 'MULTIMEDIA',
      width: 350,
      brand: 'CENTRAL',
      subtitle: 'MULTIMEDIA',
    },
    description: {
      text:
        'Modular y personalizable para que la configures a tu gusto. Además tiene GPS y Android auto / Apple Carplay con conexión inalámbrica.',
    },
    action: {
      text: 'Conectividad',
      align: 'right',
    },
  },
  {
    id: 6,
    image: images.Stories06,
    imageAlt: 'Stories 6 - Image',
    head: {
      titlenew: 'NUEVO',
      brand: 'ASISTENCIAS',
      title: 'A LA CONDUCCIÓN',
      width: 350,
    },
    description: {
      text:
        'Asistente de frenado Pre-colisión Sistema automático de luces altas Alerta de cambio de carril involuntario.',
    },
    action: {
      text: 'Arrastre',
      align: 'center',
    },
  },
];

export default DataStories;
