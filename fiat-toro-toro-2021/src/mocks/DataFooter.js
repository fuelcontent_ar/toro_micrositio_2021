const BtnFooter = [
  {
    id: 1,
    name: 'Comprá la tuya',
    ref: 'compre-a-sua',
    link: 'https://toro.fiat.com.ar/configurador.html',
  },
  {
    id: 2,
    name: 'Configurá la tuya',
    ref: 'monte-a-sua',
    link: 'https://toro.fiat.com.ar/configurador.html',
  },
  {
    id: 4,
    name: 'Concesionario',
    ref: 'concessionaria',
    link: 'https://www.fiat.com.ar/concesionarios.html',
  },
  // {
  //   id: 5,
  //   name: 'Ofertas Fiat',
  //   ref: 'ofertas-jeep',
  //   link: 'https://ofertas.fiat.com.br/?q=toro',
  // },
];

export default BtnFooter;
