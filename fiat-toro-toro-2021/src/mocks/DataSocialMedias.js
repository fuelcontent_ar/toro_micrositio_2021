const SocialMedias = [
  {
    id: 1,
    name: 'facebook',
    link: 'https://www.facebook.com/fiatargentina',
  },
  {
    id: 2,
    name: 'Twitter',
    link: 'https://twitter.com/fiatargentina',
  },
  {
    id: 3,
    name: 'Instagram',
    link: 'https://www.instagram.com/fiatargentina/',
  },
  {
    id: 4,
    name: 'Youtube',
    link: 'https://www.youtube.com/user/FiatAutoArgentina',
  },
];

export default SocialMedias;
