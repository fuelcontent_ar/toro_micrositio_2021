const BtnFooter = [
  {
    id: 1,
    name: 'facebook',
    link: 'https://www.facebook.com/fiatargentina',
  },
  {
    id: 2,
    name: 'twitter',
    link: 'https://twitter.com/fiatargentina',
  },
  {
    id: 3,
    name: 'instagram',
    link: 'https://www.instagram.com/fiatargentina/',
  },
  {
    id: 4,
    name: 'youtube',
    link: 'https://www.youtube.com/user/FiatAutoArgentina',
  },
];

export default BtnFooter;
