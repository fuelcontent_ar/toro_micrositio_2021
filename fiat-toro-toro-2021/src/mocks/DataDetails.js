import * as images from '../assets';

const Details = [
  {
    brand: 'Diseño',
    data: [
      {
        title: 'Nuevo Diseño',
        description:
          'De una, te va a fascinar el nuevo capot. Después, te vas a enamorar de la nueva parrilla, con el logo de Fiat y el detalle de la bandera italiana. Un diseño musculoso, que equilibra belleza y fuerza. El frente de la nueva Fiat Toro es increible.',
        image: images.DetailDesign1,
      },
      {
        title: 'Overbumper integrado',
        description:
          'Esta barra de protección integrada en el paragolpes, además de aportar personalidad, hace que Fiat Toro sea aún más imponente y robusta.',
        image: images.DetailDesign2,
      },
      {
        title: 'Faros Full LED',
        description:
          'La tecnología LED está presente en todo el conjunto óptico: faros principales, faros antiniebla, luces de dirección y posición. Además de moderno, este sistema garantiza mejor luminosidad, mayor durabilidad y más economía.',
        image: images.DetailDesign3,
      },
      {
        title: 'Llantas de 18"',
        description:
          'Presente en la version Volcano, estas llantas de 18" aportan belleza y presencia incomparables a FIAT Toro. Con neumáticos de uso mixto en la versión diésel 4x4 y de ruta en la versión nafta 4x2.',
        image: images.DetailDesign5,
      },
      {
        title: 'Llantas de 18" con neumáticos Todoterreno Plus',
        description:
          'Los neumáticos ATR son de uso mixto para que los uses en la carretera o fuera de ella. Estas ruedas harán que sus viajes off-road sean todavá mejores.',
        image: images.DetailDesign4,
      },
    ],
  },

  {
    brand: 'Interior',
    data: [
      {
        title: 'Porta-Objetos',
        description:
          'La Fiat Toro duplicó el número de espacios porta-objetos en el interior. Ahora, tenés espacio en todos los rincones para guardar cualquier cosa, te van a encantar los porta-botellas de 700 ml delante de la caja de cambios.',
        image: images.Interior1,
      },
      {
        title: 'Tablero full digital de 7”',
        description:
          'Ahora el cuadro de instrumentos es totalmente digital y podes personalizarlo para mostrar la información que vos quieras en varios diseños y colores.',
        image: images.Interior2,
      },
      {
        title: 'Consola Central',
        description:
          'Mucho mas espaciosa, con un espacio para colocar el teléfono y cargador inalámbrico de serie para todas las versiones.',
        image: images.Interior3,
      },
      {
        title: 'Volante',
        description:
          'El volante revestido en cuero suma el nuevo logo de la marca en el centro. Con los comandos vas a poder controlar la información del tablero y central multimedia. Con las levas al volante vas a poder controlar las marchas para una conducción mas deportiva.',
        image: images.Interior4,
      },
      /*  {
        title: 'Revestimento dos Bancos Ranch',
        description:
          'Os bancos e as costuras ganham uma estética exclusiva nessa versão. Totalmente em couro e tudo em marrom, para combinar com o design do carro.',
        image: images.Interior5,
      }, */
      {
        title: 'Tapizado diferente a cada versión',
        description:
          'Todas las versiones vienen con tapizado en cuero. Para la versión Ultra las costuras son en color rojo acompañando el look interno oscurecido y con detalles simil fibra de carbono.',
        image: images.Interior6,
      },
    ],
  },
  {
    brand: 'Performance',
    data: [
      {
        title: 'Motor Turbo T270 NAFTA',
        description:
          'A pesar de tener una cilindrada menor, este motor tiene 175 CV y un torque de 270 Nm, superior a otros motores mas grandes. Con este motor mas moderno vas a tener un menor consumo y mucha mas diversión, ya que a partir de 1500 RPM se activa el turbo.',
        image: images.DetailPerformace1,
      },
      {
        title: 'Motor Turbo Diesel',
        description:
          'Con alta potencia y excelente desempeño, el motor turbodiésel tiene un torque de 350Nm para salir a enfrentar los terrenos mas off-road.',
        image: images.DetailPerformace2,
      },
      {
        title: 'Tracción 4x4',
        description:
          'En terrenos complicados de baja velocidad podes activar el 4WD low para que la camioneta arranque mas lento, estire mas los cambios y desactive el control de tracción y estabilidad para reducir la intervención electrónica.',
        image: images.DetailPerformace3,
      },
    ],
  },
  {
    brand: 'Tecnología',
    data: [
      {
        title: 'Central Multimedia 10.1”',
        description:
          'Las versiones Freedom vienen con una central multimedia horizontal de 8,4" y el resto con una gran central multimedia de 10,1" vertical. Además viene con GPS y es modular para que la personalices con la información que quieras.',
        image: images.DetailTecnologia1,
      },
      {
        title: 'Climatizador Bi-zona',
        description:
          'Para que el conductor y el acompañante pueda seleccionar cada uno su temperatura. Ahora además podes controlar todo esto desde la central multimedia.',
        image: images.DetailTecnologia2,
      },
      {
        title: 'Cargador Inalámbrico',
        description:
          'En el espacio delante de la palanca de cambios vas a poder apoyar tu teléfono para que se cargue si tiene esta opción. Además de eso, tenes un USB tipo A y C para conectar cualquier otro dispositivo.',
        image: images.DetailTecnologia3,
      },
    ],
  },
  {
    brand: 'Seguridad',
    data: [
      {
        title: 'Asistente de frenado Pre-colisión',
        description:
          'A través de sensores, el sistema reconoce si hay un obstáculo delante del vehículo y activa automáticamente los frenos para evitar un accidente. Tecnología y seguridad: unidas en la Fiat Toro. El grado de lejanía al cual se activa puede configurarse desde la central multimedia.',
        image: images.DetailSeguranca1,
      },
      {
        title: 'Sistema automático de luces altas',
        description:
          'La cámara frontal de la Fiat Toro identifica si viene un vehículo de frente y baja automáticamente las luces altas para no encandilarlo.',
        image: images.DetailSeguranca2,
      },
      {
        title: 'Alerta de cambio de carril involuntario',
        description:
          'La Fiat Toro siempre esta alerta para ir detectando el camino por el que circula y corregir la dirección en caso te vayas de carril. El grado de intervención puede configurarse desde la central multimedia.',
        image:  images.DetailSeguranca3,
      }, 
      {
        title: 'Sensor frontal de estacionamento',
        description:
          'Estacionar ahora es mucho mas fácil, asistido por la cámara de reversa, los sensores de estacionamiento traseros y delanteros.',
        image: images.DetailSeguranca4,
      },
      {
        title: '7 Airbags de serie',
        description:
          'Los airbags frontales, laterales, de cortina y de rodilla vienen de serie para todas las versiones. Para que viajes siempre seguro.',
        image: images.DetailSeguranca5,
      },
      /*  {
        title: 'Isofix',
        description:
          'A segurança da Fiat Toro começa com seus filhos e filhas. Essa trava é muito prática para fixar a cadeirinha das crianças ou o bebê conforto no banco traseiro.',
        image: images.DetailSeguranca6,
      }, */
      {
        index: 'Seguridad',
        title: 'Kit de Seguridad',
        description:
          'Este vehículo es entregado en el concesionario oficial con un kit de seguridad y cubre alfombras homologadas.',
        image: images.DetailSeguranca6,
        next: 'Accesorios',
      },
    ],
  },
  {
    brand: 'Accesorios',
    id: 5,
    data: [
      {
        index: 'Accesorios',
        title: 'Barra San Antonio Cromada',
        description:
          'Ayuda en la retención y organización de cargas además de brindar mayor robustez y personalizacíon a la caja.',
        image: images.DetailAcessorios1,
        link: 'https://servicios.fiat.com.ar/customizacion.html',
        btn: 'Saber más',
      },
      {
        index: 'Accesorios',
        title: 'Barra San Antonio negra​',
        description:
          'También disponible en negro para que elijas la que mas te gusta. Ayuda en la retención y organización de cargas además de brindar mayor robustez y personalización a la caja.​',
        image: images.DetailAcessorios2,
        link: 'https://servicios.fiat.com.ar/customizacion.html',
        btn: 'Saber más',
      },
      {
        index: 'Accesorios',
        title: 'Barra San Antonio negra',
        description:
          'Para que sigas personalizando tu camioneta como mas te guste, agrega robustez visual y un toque de elegancia.',
        image: images.DetailAcessorios3,
        link: 'https://servicios.fiat.com.ar/customizacion.html',
       btn: 'Saber más',
      },
      {
        index: 'Accesorios',
        title: 'Estribo lateral negro',
        description:
          'También disponible en color negro para que lo combines con el resto.',
        image: images.DetailAcessorios4,
        link: 'https://servicios.fiat.com.ar/customizacion.html',
       btn: 'Saber más',
      },
      {
        index: 'Accesorios',
        title: 'Enganche de remolque',
        description:
          'Para que puedas llevar tu tráiler y cargar todas tus aventuras. Cundo no lo usas, simplemente quitas la parte móvil de la bocha y circulas sin problema.',
        image: images.DetailAcessorios5,
        link: 'https://servicios.fiat.com.ar/customizacion.html',
       btn: 'Saber más',
      },
    ],
  },
  {
    brand: 'Paquete de Servicio',
    data: [
      {
        title: 'El cuidado que merece tu FIAT Toro',
        description:
          'Descubrí los paquetes de servicio FIAT Mopar. Tu FIAT Toro puede tener los services personalizados con descuentos y precios fijos.',
        image: images.DetailPacote1,
        link: 'https://servicios.fiat.com.ar/',
        btn: 'Compre ahora',
      },
    /*   {
        index: 'Paquete de Servicio',
        title: 'Garantia adicional FIAT',
        description:
          'Sua FIAT Toro pode ter até 5 anos de garantia, além dos 3 anos de fábrica. Você pode comprar mais 1 ou 2 anos de garantia, com planos a partir de R$ 2 por dia.',
        image: images.DetailPacote2,
        link: 'https://servicios.fiat.com.ar/',
        btn: 'Compre ahora',
      }, */
      {
        index: 'Paquete de Servicio',
        title: 'MOPAR Assistance',
        description:
          'El Vehículo cuenta con cobertura durante el primer año de uso, en cualquier lugar de la república Argentina y en países limítrofes.',
        image: images.DetailPacote3,
        link:
          'https://servicios.fiat.com.ar/',
        btn: 'Compre ahora',
      },
 /*      {
        index: 'Paquete de Servicio',
        title: 'Proteção de pneus FIAT',
        description:
          'Novo e exclusivo serviço da Fiat para a proteção de pneus contra avarias e acidentes, com reposição garantida e cobertura de 12 meses.',
        image: images.DetailPacote4,
        link: 'https://servicios.fiat.com.ar/',
        btn: 'Compre ahora',
      }, */
    ],
  },
];

export default Details;
