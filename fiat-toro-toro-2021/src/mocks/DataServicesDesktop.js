import * as images from '../assets';

const DataServicesDesktop = [
  {
    id: 1,
    image: {
      desktop: images.serviceDesktop.serviceFrontal,
    },
    imageAlt: 'Service Info Image',
    header: {
      text: 'A PICAPE MAIS INTELIGENTE DO BRASIL',
    },
    description: {
      desktop: 'Conheça os serviços do',
      bold: 'Connect Me',
    },
    next: 'Comandos á distância',
  },
  {
    id: 2,
    image: {
      desktop: images.serviceDesktop.serviceComands,
    },
    imageAlt: 'Service Map Image',
    header: {
      text: 'OPERAÇÕES REMOTAS',
    },
    description: {
      desktop:
        'Pelo smartphone, smartwatch ou Alexa, você pode travar e destravar as portas, acender os faróis e até ligar e desligar o carro, mesmo a quilômetros de distância.',
    },
    next: 'Localização remota',
  },
  {
    id: 3,
    image: {
      desktop: images.serviceDesktop.serviceLocation,
    },
    imageAlt: 'Service Distance Image',
    header: {
      text: 'LOCALIZAÇÃO DO VEÍCULO',
    },
    description: {
      desktop:
        'Esqueceu onde parou? Em estacionamentos lotados, por exemplo, você descobre a localização exata da Nova Fiat Toro e pode traçar uma rota a pé até o carro.',
    },
    next: 'Alertas de direção',
  },
  {
    id: 4,
    image: {
      desktop: images.serviceDesktop.serviceAlert,
    },
    imageAlt: 'Service Mechanical Image',
    header: {
      text: 'ALERTAS DE CONDUÇÃO',
    },
    description: {
      desktop:
        'Ao emprestar a Nova Fiat Toro, você pode estabelecer limites de velocidade e perímetro. Caso a pessoa ultrapasse esses limites, você recebe um alerta.',
    },
    next: 'Sos automático',
  },
  {
    id: 5,
    image: {
      desktop: images.serviceDesktop.serviceSos,
    },
    imageAlt: 'Service Airbook Image',
    header: {
      text: 'CHAMADA AUTOMÁTICA DE EMERGÊNCIA',
    },
    description: {
      desktop:
        'Em caso de acidente com acionamento dos airbags, o sistema liga automaticamente para a central e passa a localização e informações básicas do veículo.',
    },
    next: 'Alerta de roubo',
  },
  {
    id: 6,
    image: {
      desktop: images.serviceDesktop.serviceRoubo,
    },
    imageAlt: 'Service Roubo Image',
    header: {
      text: 'ALERTA PREVENTIVO DE FURTO',
    },
    description: {
      desktop:
        'Caso exista uma possibilidade de furto, o sistema envia notificações para o seu celular. Se você confirmar que não está com a Toro, a central é acionada e o carro é rastreado.',
    },
    next: 'Navegação inteligente',
  },
  {
    id: 7,
    image: {
      desktop: images.serviceDesktop.serviceNav,
    },
    imageAlt: 'Service Roubo Image',
    header: {
      text: 'MAPA INTELIGENTE',
    },
    description: {
      desktop:
        'Além das rotas customizadas, a navegação apresenta informações de acordo com os indicadores do carro. Se o tanque estiver vazio, por exemplo, ela indica o posto de gasolina mais próximo.',
    },
    next: 'A picape mais inteligente do Brasil',
  },
];

export default DataServicesDesktop;
