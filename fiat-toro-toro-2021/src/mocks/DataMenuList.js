const MenuList = [
  {
    id: 1,
    name: 'Novedades Toro',
    ref: 'toro-2022',
    link: '#toro-2022',
  },
  {
    id: 3,
    name: 'Versiones',
    ref: 'todas-as-versoes',
    link: '#versions',
  },
  {
    id: 4,
    name: 'Fotos',
    ref: 'galeria',
    link: '#galeria',
  },
  {
    id: 5,
    name: 'Diseño',
    ref: 'Design',
    link: '#design',
  },
  {
    id: 6,
    name: 'Interior',
    ref: 'interior',
    link: '#interior',
    accordion: true,
  },
  {
    id: 7,
    name: 'Performance',
    ref: 'performance',
    link: '#performance',
    accordion: true,
  },
  {
    id: 8,
    name: 'Tecnología',
    ref: 'tecnologia',
    link: '#tecnologia',
    accordion: true,
  },
  {
    id: 9,
    name: 'Seguridad',
    ref: 'seguranca',
    link: '#seguranca',
    accordion: true,
  },
  {
    id: 10,
    name: 'Accesorios',
    ref: 'acessorios',
    link: '#Acessórios',
    accordion: true,
  },
  {
    id: 11,
    name: 'Paquete de servicios',
    ref: 'pacotes-de-servicos',
    link: '#Pacotes de serviços',
    accordion: true,
  },
];

export default MenuList;
