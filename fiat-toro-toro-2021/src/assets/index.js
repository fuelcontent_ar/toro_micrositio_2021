import chevronBotton from './images/chevronBotton.svg';
import LogoFiat2x from './images/LogoFiat2x.png';
import Seta from './images/seta.svg';
import Face from './images/Icons/Face.png';
import Twitter from './images/Icons/Twitter.png';
import Insta from './images/Icons/Insta.png';
import Youtube from './images/Icons/YouTube.png';
import Stories01 from './images/Stories/Stories01.jpg';
import Stories02 from './images/Stories/Stories02.jpg';
import Stories03 from './images/Stories/Stories03.jpg';
import Stories04 from './images/Stories/Stories04.jpg';
import Stories05 from './images/Stories/Stories05.jpg';
import Stories06 from './images/Stories/Stories06.jpg';
import ServiceInfo from './images/ServiceCar/ServiceInfo.jpg';
import ServiceMap from './images/ServiceCar/ServiceMap.jpg';
import ServiceDistance from './images/ServiceCar/ServiceDistance.jpg';
import ServiceAirbook from './images/ServiceCar/ServiceAirbook.jpg';
import ServiceRoubo from './images/ServiceCar/ServiceRoubo.jpg';
import ServiceSos from './images/ServiceCar/ServiceSos.jpg';
import ServiceNavegaçao from './images/ServiceCar/ServiceNavegaçao.jpg';



import toroVolcano from './images/VersionsCar/toroVolcano.png';
import GalleryV1 from './images/Gallery/GalleryV1.jpg';
import GalleryV2 from './images/Gallery/GalleryV2.jpg';
import GalleryV3 from './images/Gallery/GalleryV3.jpg';
import GalleryV4 from './images/Gallery/GalleryV4.jpg';
import GalleryV5 from './images/Gallery/GalleryV5.jpg';
import GalleryV6 from './images/Gallery/GalleryV6.jpg';

import GalleryU1 from './images/Gallery/GalleryU1.jpg';
import GalleryU2 from './images/Gallery/GalleryU2.jpg';
import GalleryU3 from './images/Gallery/GalleryU3.jpg';
import GalleryU4 from './images/Gallery/GalleryU4.png';
import GalleryU5 from './images/Gallery/GalleryU5.jpg';
import GalleryU6 from './images/Gallery/GalleryU6.jpg';

import toroFreedom from './images/VersionsCar/toroFreedom.png';
import bgFreedom from './images/VersionsCar/bgFreedom.jpg';
import GalleryF1 from './images/Gallery/GalleryF1.jpg';
import GalleryF2 from './images/Gallery/GalleryF2.jpg';
import GalleryF3 from './images/Gallery/GalleryF3.jpg';
import GalleryF4 from './images/Gallery/GalleryF4.jpg';
import GalleryF5 from './images/Gallery/GalleryF5.jpg';
import GalleryF6 from './images/Gallery/GalleryF6.jpg';

import DetailPerformace1 from './images/Details/DetailPerformace1.jpg';
import DetailPerformace2 from './images/Details/DetailPerformace2.jpg';
import DetailPerformace3 from './images/Details/DetailPerformace3.jpg';

import Interior1 from './images/Details/Interior1.jpg';
import Interior2 from './images/Details/Interior2.jpg';
import Interior3 from './images/Details/Interior3.jpg';
import Interior4 from './images/Details/Interior4.jpg';
import Interior5 from './images/Details/Interior5.jpg';
import Interior6 from './images/Details/Interior6.jpg';

import DetailTecnologia1 from './images/Details/DetailTecnologia1.jpg';
import DetailTecnologia2 from './images/Details/DetailTecnologia2.jpg';
import DetailTecnologia3 from './images/Details/DetailTecnologia3.jpg';

import DetailDesign1 from './images/Details/Detail1.jpg';
import DetailDesign2 from './images/Details/Detail2.jpg';
import DetailDesign3 from './images/Details/Detail3.jpg';
import DetailDesign4 from './images/Details/Detail4.jpg';
import DetailDesign5 from './images/Details/Detail5.jpg';

import DetailSeguranca1 from './images/Details/DetailSeguranca1.jpg';
import DetailSeguranca2 from './images/Details/DetailSeguranca2.jpg';
import DetailSeguranca3 from './images/Details/DetailSeguranca3.jpg';
import DetailSeguranca4 from './images/Details/DetailSeguranca4.jpg';
import DetailSeguranca5 from './images/Details/DetailSeguranca5.jpg';
import DetailSeguranca6 from './images/Details/DetailSeguranca6.jpg';

import DetailAcessorios1 from './images/Details/DetailAcessorios1.jpg';
import DetailAcessorios2 from './images/Details/DetailAcessorios2.jpg';
import DetailAcessorios3 from './images/Details/DetailAcessorios3.jpg';
import DetailAcessorios4 from './images/Details/DetailAcessorios4.jpg';
import DetailAcessorios5 from './images/Details/DetailAcessorios5.jpg';

import DetailPacote1 from './images/Details/DetailPacote1.jpg';
import DetailPacote2 from './images/Details/DetailPacote2.jpg';
import DetailPacote3 from './images/Details/DetailPacote3.jpg';
import DetailPacote4 from './images/Details/DetailPacote4.jpg';

import CardMotor from './images/Cards/CardMotor.jpg';
import CardRodas from './images/Cards/CardRodas.jpg';
import CardAirBag from './images/Cards/CardAirBag.jpg';
import CardFarol from './images/Cards/CardFarol.jpg';
import CardMultimidia from './images/Cards/CardMultimidia.jpg';
import CardMultimidia2 from './images/Cards/CardMultimidia2.jpg';
import CardCapota from './images/Cards/CardCapota.jpg';
import CardSanto from './images/Cards/CardSanto.jpg';
import CardEsc from './images/Cards/CardEsc.jpg';
import CardMultimidia8 from './images/Cards/CardMultimidia8.jpg';
import CardWireless from './images/Cards/CardWireless.jpg';
import CardMaçaneta from './images/Cards/CardMaçaneta.jpg';
import CardFrontal from './images/Cards/CardFrontal.jpg';
import CardBanco from './images/Cards/CardBanco.jpg';

import DownloadEndurance from './images/DownloadPics/FiatToro_endurance.jpg';
import DownloadFreedom from './images/DownloadPics/FiatToro_freedom.jpg';
import DownloadRanch from './images/DownloadPics/FiatToro_ranch.jpg';
import DownloadUltra from './images/DownloadPics/FiatToro_ultra.jpg';
import DownloadVolcano from './images/DownloadPics/FiatToro_volcano.jpg';

import toroUltra from './images/VersionsCar/toroUltra.png';
import bgUltra from './images/VersionsCar/bgUltra.jpg';

import bgVolcano from './images/VersionsCar/bgVolcano.jpg';

import HandIcon from './images/Icons/hand.svg';
import Title from './images/Icons/Title.svg';
import SymbolJeep from './images/Symbol/SymbolJeep.svg';
import LeftArrow from './images/Icons/LeftArrow.svg';
import RightArrow from './images/Icons/RightArrow.svg';
import Download from './images/Icons/download.svg';

import * as hero from './images/hero';
import * as serviceDesktop from './images/serviceDesktop';
import * as aventures from './images/aventures';
import * as GalleryDesktop from './images/GalleryDesktop';
import bgFooter from './images/bgFooter.png';
import FiatGreen from './images/FiatGreen.svg';
import Flag from './images/Icons/flag.svg';
import FlagGreen from './images/Icons/flagGreen.svg';
import FlagColorida from './images/Icons/flagColorida.svg';
import LogoConnect from './images/logo-connect.svg';
import Zoom from './zoom.svg';

export {
  Zoom,
  GalleryF2,
  GalleryF3,
  GalleryF5,
  DownloadEndurance,
  DownloadFreedom,
  DownloadRanch,
  DownloadUltra,
  DownloadVolcano,
  FlagGreen,
  LogoConnect,
  GalleryF1,
  GalleryF4,
  GalleryF6,
  GalleryU1,
  GalleryU2,
  GalleryU3,
  GalleryU4,
  GalleryU5,
  GalleryU6,

  GalleryV1,
  GalleryV2,
  GalleryV3,
  GalleryV4,
  GalleryV5,
  GalleryV6,

  Download,
  FlagColorida,
  Stories06,
  ServiceNavegaçao,
  ServiceRoubo,
  ServiceSos,
 
  CardBanco,
  CardFrontal,
  bgVolcano,
  toroVolcano,
  CardMaçaneta,
  CardWireless,
  bgFreedom,
  toroFreedom,
  CardMultimidia8,
  DetailPacote1,
  Interior1,
  Interior2,
  Interior3,
  Interior4,
  Interior5,
  Interior6,
  FiatGreen,
  toroUltra,
  bgUltra,

  CardSanto,
  CardCapota,
  Seta,
  Flag,
  chevronBotton,
  SymbolJeep,
  HandIcon,
  Title,
  DetailPacote2,
  DetailPacote3,
  DetailPacote4,
  DetailAcessorios1,
  DetailAcessorios2,
  DetailAcessorios3,
  DetailAcessorios4,
  DetailAcessorios5,
  DetailSeguranca1,
  DetailSeguranca2,
  DetailSeguranca3,
  DetailSeguranca4,
  DetailSeguranca5,
  DetailSeguranca6,
  DetailDesign5,
  DetailDesign4,
  CardAirBag,
  CardFarol,
  CardMultimidia,
  CardMultimidia2,
  CardEsc,
  CardMotor,
  CardRodas,
  LogoFiat2x,
  Face,
  Twitter,
  Insta,
  Youtube,
  Stories01,
  Stories02,
  Stories03,
  Stories04,
  Stories05,
  ServiceInfo,
  ServiceMap,
  ServiceDistance,
  ServiceAirbook,
  DetailDesign1,
  DetailDesign2,
  DetailDesign3,
  DetailPerformace1,
  DetailPerformace2,
  DetailPerformace3,
  DetailTecnologia1,
  DetailTecnologia2,
  DetailTecnologia3,
  LeftArrow,
  RightArrow,
  hero,
  serviceDesktop,
  bgFooter,
  aventures,
  GalleryDesktop,
};
