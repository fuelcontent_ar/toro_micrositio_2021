import styled, { css } from 'styled-components';
import { Swiper as SwiperReact } from 'swiper/react';
import Colors from '../../../styles/colors';

export const Swiper = styled(SwiperReact)`
  background-color: ${Colors.greyMedium};
  margin-top: -1px;

  .swiper-button-next,
  .swiper-button-prev {
    background-color: ${Colors.success};
    border-radius: 5px;
    color: ${Colors.white};
    height: 44px;
    top: 404px;
    width: 44px;
    z-index: 99999;

    &::after {
      font-size: 18px;
    }

    ${props =>
      props.onBlock &&
      css`
        position: fixed;
        top: 400px;
      `}

    ${props =>
      props.afterBlock &&
      css`
        top: calc(100% - 300px);
      `}
  }

  .swiper-button {
    &-next {
      right: 0;
    }

    &-prev {
      left: 0;
    }
  }

  .swiper-pagination {
    align-items: center;
    bottom: calc(100% - 526px);
    display: flex;
    justify-content: center;
    height: 235px;

    &-bullet {
      background-color: #83837c;
      border-radius: 30%;
      height: 4px;
      opacity: 0.5;
      width: 4px;

      &-active {
        background-color: ${Colors.primary};
        height: 7px;
        opacity: 1;
        width: 7px;
      }
    }
  }
`;

export const Content = styled.div`
  height: 330px;
  background-image: url(${props => props.bg && props.bg});
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  color: ${Colors.white};
  margin-top: -1px;
  position: relative;

  &::before {
    background-color: ${Colors.black};
    content: '';
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: -1;
  }

  &::after {
    content: '';
    background-color: ${Colors.greyMedium};
    bottom: -106px;
    display: block;
    height: 120px;
    position: absolute;
    width: 100%;
  }

  h5 {
    font-size: 16px;
    font-weight: 400;
    letter-spacing: 0.16px;
    line-height: 20px;
    padding-top: 30px;
    text-align: center;
    font-family: 'FuturaPT Light', sans-serif;

    span {
      font-size: inherit;
      font-family: 'FuturaPT Bold', sans-serif;
      letter-spacing: inherit;
    }
  }
`;

export const BoxSection = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
  padding: 14px 40px;

  img:first-child {
    margin-bottom: 10px;
    width: 54px;
    height: 30px;
  }
  div:first-child {
    display: flex;
    flex-direction: column;
  }

  button:first-child {
    background-color: transparent;
    font-size: 20px;
    margin-top: -60px;
  }
  a:first-child {
    background-color: ${Colors.white};
    border-radius: 3px;
    font-size: 12px;
    height: 18px;
    margin-top: 3px;
    padding-top: 3px;
    text-align: center;
    width: 20px;
  }
  h3 {
    font-size: 18px;
    font-weight: 400;
    font-family: 'FuturaPT Light', sans-serif;
  }

  h1 {
    font-size: 30px;
    letter-spacing: 0.3px;
    line-height: 30px;
    font-family: 'FuturaPT Bold', sans-serif;
  }

  h2 {
    font-size: 30px;
    letter-spacing: 0.3px;
    line-height: 30px;
    width: ${props => props.width && props.width}px;
    font-family: 'FuturaPT Bold', sans-serif;
  }
  div:last-child {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 44px;
    height: 46px;

    a {
      background-color: transparent;
      padding: 0;
      img {
        width: 20px;
        height: 20px;
      }
    }
  }
`;

export const Model = styled.div`
  display: flex;
  text-align: center;
  justify-content: center;

  h4 {
    font-size: 20px;
    letter-spacing: 0.2px;
    margin: 44px 0 28px;
    font-family: 'FuturaPT Bold', sans-serif;
  }

  span {
    font-size: 20px;
    font-weight: 400;
    text-transform: uppercase;
  }
`;

export const Car = styled.img`
  margin: 0 auto;
  margin-bottom: 16px;
  max-width: 375px;
  position: relative;
  top: -20px;
  width: 100%;
  z-index: 1;
`;

export const Cards = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 120px;
  position: relative;
`;

export const Infos = styled.div`
  background-color: #ddd7c7;
  display: flex;
  flex-direction: column;
  max-width: 100%;
  width: 94px;
  border-radius: 0px 0px 4px 4px;

  & + div {
    margin-left: 8px;
  }

  img {
    border-radius: 0px;
    height: 85px;
    object-fit: cover;
    width: 100%;
  }

  span {
    color: ${Colors.black};
    font-family: 'FuturaPT Bold', sans-serif;
    font-size: ${props => (props.size ? props.size : 10)}px;
    letter-spacing: 0.15px;
    line-height: 1;
    padding: 6px;
    width: 100%;
    text-align: center;
  }
`;

export const Gallery = styled.div`
  align-content: space-between;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  height: 400px;
  margin: 0 auto;
  max-width: 296px;

  > img {
    height: auto !important;
    width: 145px !important;
  }

`;

export const Image = styled.img`
  border-radius: 8px;
  height: ${props => (props.mini ? 112 : 156)}px;
  margin-bottom: 12px;
  width: calc(50% - 6px);
  object-fit: cover;
`;

export const actionsContainer = styled.div`
  img {
    margin-top: 200px;
  }
`;

export const BoxShare = styled.div`
  position: absolute;
  right: 27px;
  bottom: 190px;
  img {
    height: 20px;
  }
`;
