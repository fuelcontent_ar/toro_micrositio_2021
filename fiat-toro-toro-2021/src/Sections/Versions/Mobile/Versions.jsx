import React, { useEffect, useState } from 'react';
import SwiperCore, {
  Navigation,
  Pagination,
  A11y,
} from 'swiper';
import { SwiperSlide } from 'swiper/react';
import ImageZoom from 'react-medium-image-zoom';
import {
  BoxSection,
  Cards,
  Swiper,
  Infos,
  Gallery,
  Model,
  Content,
  Car,
  // Image,
} from './styles';
import 'swiper/swiper.min.css';
import 'swiper/components/navigation/navigation.min.css';
import 'swiper/components/pagination/pagination.min.css';
import * as images from '../../../assets';

SwiperCore.use([Navigation, Pagination, A11y]);

const Versions = ({ data }) => {
  const [scrollTop, setScrollTop] = useState();
  const [onBlock, setOnBlock] = useState(false);
  const [afterBlock, setAfterBlock] = useState(false);

  useEffect(() => {
    const onScroll = e => {
      setScrollTop(e.target.documentElement.scrollTop);
    };

    window.addEventListener('scroll', onScroll);

    const boxVersions = document.querySelector('#versions')
      .offsetTop;
    const heightVersions = document.querySelector('#versions')
      .offsetHeight;

    const checkAfterBlock =
      scrollTop > boxVersions + heightVersions - 650;

    const checkOnBlock =
      scrollTop >= boxVersions + 10 && !checkAfterBlock;

    setOnBlock(checkOnBlock);
    setAfterBlock(checkAfterBlock);

    return () => window.removeEventListener('scroll', onScroll);
  }, [scrollTop]);

  return (
    <Swiper
      id="versions"
      navigation
      loop
      pagination={{ clickable: true }}
      onBlock={onBlock}
      afterBlock={afterBlock}
    >
      {data.map((item, index) => (
        <SwiperSlide id='printSlide' key={item.id}>
          <Content
            key={item.id}
            width={item.presentation.width}
            bg={item.presentation.bg}
          >
            <span style={{display: "none"}} id="carName">{item.version}</span>
            <h5>
              TODAS LAS VERSIONES <br />
              <span>DE LA FIAT TORO</span>
            </h5>

            <BoxSection>
              <div id="versionName">
                <img src={images.Flag} alt="logo" />

                <h3>FIAT TORO</h3>

                <h1>{item.presentation.title}</h1>

                <h2>{item.presentation.subtitle}</h2>
              </div>
              <div>
                  <a href={item.presentation.download} rel="noreferrer" target='_blank' download={item.version}>
                    <img src={images.Download} alt='Download'/>
                  </a>
              </div>

            </BoxSection>

            <Car src={item.presentation.image} alt="image" />
          </Content>

          <Cards>
            {item.details.map(detail => (
              <Infos>
                <img src={detail.bg} alt={detail.text} />

                <span>{detail.text}</span>
              </Infos>
            ))}
          </Cards>

          <Model>
            <h4>
              {item.version}
              <span> en todos los ángulos</span>
            </h4>
          </Model>
          <Gallery id="galeria">
            {item.gallery?.map(photo => (
              <ImageZoom
                  image={{
                    src: photo.photos,
                    alt: "photos",
                    className: 'img',
                    style: { 
                      borderRadius: '8px',
                      height: photo.mini ? '112' : '156px',
                      marginBottom: '12px',
                      objectFit: 'cover',
                      width: '145px',
                    }
                  }}
                  zoomImage={{
                    src: photo.photos,
                    alt: "photos",
                    className: "img--zoomed",
                  }}
                />
            ))}
          </Gallery>
        </SwiperSlide>
      ))}
    </Swiper>
  );
};
export default Versions;
