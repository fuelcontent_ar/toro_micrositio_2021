import * as images from '../../../assets';

const VersionsDesktop = [
  {
    version: 'TORO ULTRA',
    presentation: {
      image: images.GalleryDesktop.toroUltra.toroUltra,
      title: 'ULTRA',
      subtitle: 'DIESEL',
      bg: images.GalleryDesktop.toroUltra.bgUltra,
    },
    details: [
      {
        bg: images.GalleryDesktop.toroUltra.card1,
        text: 'Capota rígida',
      },
      {
        bg: images.GalleryDesktop.toroUltra.card2,
        text: 'Barra san Antonio integrada',
      },
      {
        bg: images.GalleryDesktop.toroUltra.card3,
        text: 'Central Multimedia 10.1',
      },
    ],
    id: 1,
    gallery: [
      {
        photos: images.GalleryDesktop.toroUltra.toroUltra1,
        photoId: 1,
      },
      {
        photos: images.GalleryDesktop.toroUltra.toroUltra2,
        photoId: 2,
      },
      {
        photos: images.GalleryDesktop.toroUltra.toroUltra3,
        photoId: 3,
      },
      {
        photos: images.GalleryDesktop.toroUltra.toroUltra4,
        photoId: 4,
      },
      {
        photos: images.GalleryDesktop.toroUltra.toroUltra5,
        photoId: 5,
      },
      {
        photos: images.GalleryDesktop.toroUltra.toroUltra6,
        photoId: 6,
      },
    ],
  },
  {
    version: 'TORO VOLCANO',
    presentation: {
      image: images.GalleryDesktop.toroVolcano.toroVolcano,
      title: 'VOLCANO',
      subtitle: 'DIESEL NAFTA',
      bg: images.GalleryDesktop.toroVolcano.bgVolcano,
    },
    details: [
      {
        bg: images.GalleryDesktop.toroVolcano.card1,
        text: 'ADAS',
      },
      {
        bg: images.GalleryDesktop.toroVolcano.card2,
        text: 'Apertura y encendido remoto',
      },
      {
        bg: images.GalleryDesktop.toroVolcano.card3,
        text: 'LLantas de aleación 18”',
      },
    ],
    id: 2,
    gallery: [
      {
        photos: images.GalleryDesktop.toroVolcano.toroVolcano1,
        photoId: 1,
      },
      {
        photos: images.GalleryDesktop.toroVolcano.toroVolcano2,
        photoId: 2,
      },
      {
        photos: images.GalleryDesktop.toroVolcano.toroVolcano3,
        photoId: 3,
      },
      {
        photos: images.GalleryDesktop.toroVolcano.toroVolcano4,
        photoId: 4,
      },
      {
        photos: images.GalleryDesktop.toroVolcano.toroVolcano5,
        photoId: 5,
      },
      {
        photos: images.GalleryDesktop.toroVolcano.toroVolcano6,
        photoId: 6,
      },
    ],
  },
  {
    version: 'TORO FREEDOM',
    presentation: {
      image: images.GalleryDesktop.toroFreedom.toroFreedom,
      title: 'FREEDOM',
      subtitle: 'DIESEL NAFTA',
      bg: images.GalleryDesktop.toroFreedom.bgFreedom,
    },
    details: [
      {
        bg: images.GalleryDesktop.toroFreedom.card1,
        text: 'Cargador inalámbrico',
      },
      {
        bg: images.GalleryDesktop.toroFreedom.card2,
        text: 'Central multimedia 8,4”',
      },
      {
        bg: images.GalleryDesktop.toroFreedom.card3,
        text: 'Faros Full LED”',
      },
    ],
    id: 3,
    gallery: [
      {
        photos: images.GalleryDesktop.toroFreedom.toroFreedom1,
        photoId: 1,
      },
      {
        photos: images.GalleryDesktop.toroFreedom.toroFreedom2,
        photoId: 2,
      },
      {
        photos: images.GalleryDesktop.toroFreedom.toroFreedom3,
        photoId: 3,
      },
      {
        photos: images.GalleryDesktop.toroFreedom.toroFreedom4,
        photoId: 4,
      },
      {
        photos: images.GalleryDesktop.toroFreedom.toroFreedom5,
        photoId: 5,
      },
      {
        photos: images.GalleryDesktop.toroFreedom.toroFreedom6,
        photoId: 6,
      },
    ],
  },
  
];

export default VersionsDesktop;
