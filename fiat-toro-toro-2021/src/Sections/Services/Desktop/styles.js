import styled, { css } from 'styled-components';
import { Swiper as SwiperReact } from 'swiper/react';
import { Colors } from '../../../styles';
import { device } from '../../../styles/device';
import { FiChevronDown } from 'react-icons/fi';
import * as images from '../../../assets';

export const Container = styled.div`
  overflow: hidden;
  background-image: url(${images.serviceDesktop.bgServices});
  background-repeat: no-repeat;
  background-size: cover;
  height: 616px;
  display: flex;
  flex-direction: column;
`;

export const Swiper = styled(SwiperReact)`
  width: 100vw;
  .swiper-button {
    &-next,
    &-prev {
      background-color: transparent;
      background-repeat: no-repeat;
      background-position: center;
      border: 1px solid ${Colors.primary};
      border-radius: 4px;
      color: ${Colors.white};
      top: 464px;
      margin-top: -60px;
      width: 44px;
      margin-left: 4vw;
      transition: all 0.3s ease-in;
      z-index: 1000;
      &::after {
        font-size: 18px;
      }
    }
    &-next {
      left: 13vw;
      top: 27vw;
      @media (min-width: 1366px) {
        ${props =>
          props.open &&
          css`
            margin-left: -0.5vw;
          `}
        top: 29.5vw;
      }
      @media (min-width: 1400px) {
        left: 6.5vw;
        top: 25vw;
      }
      @media (min-width: 1440px) {
        left: 11.5vw;
        ${props =>
          props.open &&
          css`
            left: 10vw;
        `}
        top: 25vw;
      }
    }
 
    &-prev {
      left: 8.7vw;
      top: 27vw;
      @media (min-width: 1366px) {
        margin-left: 4.5vw;
        ${props =>
          props.open &&
          css`
            margin-left: 0vw;
          `}
        top: 29.5vw;
      }
      @media (min-width: 1400px) {
        left: 3.5vw;
        top: 25vw;
      }
      @media (min-width: 1440px) {
        left: 7.4vw;
        ${props =>
          props.open &&
          css`
          left: 6.5vw;
        `}
        top: 25vw;
      }
    }
  }

  .swiper-pagination {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 44px;
    margin-bottom: 185px;
    width: 37%;
    @media (min-width: 1366px) {
      height: 10px;
      display: flex;
      align-items: flex-start;
      justify-content: flex-start;
      margin-left: 13vw;
      ${props =>
        props.open &&
        css`
          margin-left: 8.5vw;
        `}
    }
    @media (min-width: 1400px) {
        top: 20vw;
        left: -5vw;
      }
    @media (min-width: 1440px) {
      height: 80px;
      display: flex;
      align-items: flex-start;
      justify-content: flex-start;
      margin-left: 16.5vw;
      ${props =>
        props.open &&
        css`
          margin-left: 11.2vw;
      `}
      top: 19.5vw;
    }

    &-bullet {
      background-color: ${Colors.white};
      border-radius: 2px;
      bottom: 64px;
      height: 6px;
      margin: 0;
      opacity: 1;
      width: 6px;

      &-active {
        background-color: ${Colors.primary};
        height: 7px;
        opacity: 1;
        width: 7px;
      }

      & + span {
        margin-left: 8px;
      }
    }
  }

 
`;

export const BoxSection = styled.div`
  width: ${props => (props.open ? 89 : 100)}vw;
  margin-top: -35px;

  @media (min-width: 1440px) and (min-height: 900px) {
    width: ${props => (props.open ? 90 : 100)}vw;
    margin: -10px auto 0 24px;
  }
  & > div {
    margin: 0 auto;
    position: relative;
    width: max-content;

    img {
      margin-top: -25px;
      max-width: 100%;
      margin-left: ${props => (props.open ? 20 : 50)}px;
    }

    div {
      color: ${Colors.white};
      position: absolute;
      width: 100%;
      margin-top: 20px;

      h1 {
        font-family: 'FuturaPT Bold', sans-serif;
      }

      p {
        font-family: 'FuturaPT Light', sans-serif;
        font-size: 18px;
        width: ${props =>
          props.highlighted ? 144 : props.highlighted}px;
      }
      strong {
        font-family: 'FuturaPT Bold', sans-serif;
      }
    }

    @media (min-width: 1400px) {
      margin-top: 0px;

      & > div {
        left: -230px;
        top: -70px;
        width: 315px;
        height: 450px;

        h1 {
          margin-top: 20px;
          margin-bottom: 40px;
        }
      }
    }
  }

  /*  ================= Desktop =================== */
  @media ${device.lg} {
    background-color: ${Colors.Brown};
    padding: 118px 0;

    &::before {
      background-color: ${Colors.Brown};
    }

    & > div {
      img {
        max-width: 614px;
        position: relative;
        left: 125px;
        top: -20px;
        border-radius: 25px;
        z-index: 10000;
        object-fit: cover;
      }

      div {
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        justify-content: space-between;
        left: -170px;
        top: -70px;
        width: 315px;
        height: 400px;

        h1 {
          font-size: 38px;
          line-height: 38px;
          width: 295px;
          position: relative;
        }
        p {
          font-family: 'Futura PT', sans-serif;
          font-size: 1rem;
          margin-top: -140px;
          width: 90%;
        }
      }
    }

    @media (min-width: 1400px) {
      & > div {
        left: -230px;
        top: -30px;
        width: 315px;
        height: 450px;

        h1 {
          margin-top: 25px;
        }

        p {
          margin-top: -126px;
        }

        span {
          padding: 10px;
        }
      }
    }
  }
`;
export const ChevronDown = styled(FiChevronDown)`
  color: ${Colors.white};
  width: 1.5em;
  height: 1.5em;
  margin: 0 auto;
  z-index: 9999;
`;

export const ArrowBox = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  width: 100vw;
  ${props =>
    props.open &&
    css`
      width: 85vw;
    `}
  margin: 0 auto;
  padding: 1rem 0rem;

  span {
    color: ${Colors.white};
    font-family: 'FuturaPT Light', sans-serif;
    font-size: 1rem;
  }
`;
