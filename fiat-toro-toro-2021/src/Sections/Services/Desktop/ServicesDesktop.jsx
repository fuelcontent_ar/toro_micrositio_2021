/* eslint-disable no-undef */
import React, { useEffect } from 'react';
import SwiperCore, {
  Navigation,
  Pagination,
  A11y,
} from 'swiper';
import { SwiperSlide } from 'swiper/react';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import { BoxSection, Swiper, Container, ArrowBox, ChevronDown,} from './styles';
import 'swiper/swiper.min.css';
import 'swiper/components/navigation/navigation.min.css';
import 'swiper/components/pagination/pagination.min.css';

import { Img } from '../../../components';

SwiperCore.use([Navigation, Pagination, A11y]);

const Services = ({ data, open }) => {
  useEffect(() => {
   
  }, []);
  return (
    <Container>
    <ArrowBox open={open}>
      <span>Scroll para saber mais</span>
      <AnchorLink href="#services">
        <ChevronDown/>
      </AnchorLink>
    </ArrowBox>
    <Swiper
      id="services"
      simulateTouch={false}
      allowTouchMove={false}
      navigation={{ clickable: true }}
      pagination
      open={open}
      loop
    >
      {data.map((item, index) => (
        <SwiperSlide open={open} key={item.id}>
          <BoxSection key={item.id} open={open} highlighted={index === 0}>
            <div>
              <Img
                md={item.image.mobile}
                lg={item.image.desktop}
                imgDefault={item.image.desktop}
              />

              <div>
                <h1>
                  {item?.header?.text}
                </h1>
                {item?.description.bold === 'Connect Me' ? <p>Conheça os serviços <br/> do  <strong> {item?.description.bold} </strong> </p> : <p> {item?.description.desktop} </p>}
                <span>Próximo: <br/> <strong>{item?.next}</strong></span>
              </div>
            </div>
          </BoxSection>
        </SwiperSlide>
      ))}
    </Swiper>
    </Container>
  );
};
export default Services;
