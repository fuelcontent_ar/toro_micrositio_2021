import React from 'react';
import Base from '../../Base';
import { TabsMobile, Stories, Cta } from '../../components';
import DataServices from '../../mocks/DataServices';
import DataStories from '../../mocks/DataStories';
import DataVersions from '../../mocks/DataVersions';
import DataDetails from '../../mocks/DataDetails';
import { ServicesMobile, VersionsMobile } from '../../Sections';

const HomeMobile = () => (
  <>
    <Base>
      <Stories stories={DataStories} />
      {/*     <ServicesMobile data={DataServices} /> */}
      <VersionsMobile data={DataVersions} />
      <TabsMobile data={DataDetails} />
      <Cta />
    </Base>
  </>
);

export default HomeMobile;
